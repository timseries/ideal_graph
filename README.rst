Ideal Graph
===========

Setup:
------

Installation::

    $ python setup.py install

Unit tests::

    $ python setup.py nosetests


Documentation:
--------------

View the documentation `online <http://ideal-graph.readthedocs.org/>`_
or build a local copy using Sphinx::

    $ python setup.py build_sphinx


Design:
-------

Specification:
~~~~~~~~~~~~~~

- Directed and undirected graphs.
- Generic nodes and edges that can support arbitrary data.
- Graph modification: add/remove nodes, edges.
- Analysis on nodes, edges: in-degree, out-degree.
- Algorithms: search, shortest path, span, centrality, connectedness, clustering. 

Assumptions:
~~~~~~~~~~~~


- Arbitrary node data fits in memory and is representable as a Python object.
- Optional edge data fits in memory and is representable as a Python object.
- Python's uuid4() always produces unique identifiers.


Software Structure:
~~~~~~~~~~~~~~~~~~~

Per the design requirements, the software is structured to allow convenient 
interface changes, interoperability with existing Python packages, 
and separability/maintainability of code. The algorithms have been
placed in separate modules according to their function in graphical analysis,
and their interfaces are independent of the graph classes. 

For instance, the k_means alogrithm takes in a feature matrix and keyword
arguments, and outputs a means matrix and with corresponding node labels
(similar to the k_means algorithm of scikitlearn). The clustering method in 
Graph can take care of standardizing the outputs of the various clustering
algortihms.


Data structures:
~~~~~~~~~~~~~~~~


Dicts or sets:
______________

The graph class stores node relationships using a dictionary of sets or dicts.
Sets and dicts both allow the efficient lookup of nodes in the graph. Sets
have the added advantage of reducing space overhead that a dictionary would
would require. If data does note need to be associated with an edge, this can 
significanlty reduce the memory required. Using a memory profiler to show 
the difference for a million-node graph, we see that the dict consumes ~50MB,
whereas the set consumes <1KB::

    >>> import guppy
    >>> hp = guppy.hpy()
    >>> num_nodes = 1000000
    >>> a = dict.fromkeys([x for x in xrange(num_nodes)])
    >>> b = set([x for x in xrange(num_nodes)])
    >>> hp.iso(a)

    Partition of a set of 1 object. Total size = 50331928 bytes.
    Index  Count   %     Size   % Cumulative  % Kind (class / dict of class)
    0      1 100 50331928 100  50331928 100 dict (no owner)

    >>> hp.iso(b)
    Partition of a set of 1 object. Total size = 232 bytes.
    Index  Count   %     Size   % Cumulative  % Kind (class / dict of class)
    0      1 100      232 100       232 100 __builtin__.set


Node:
_____

Of course, the nodes must be hashable. For this reason, a simple Node
wrapper class was included such that arbitrary data can be associated with a
unique, hashable identifier, so that even non-hashable data (such as a list)
can be associated with a node. Any downstream changes to the Node class 
attributes should be reflected in the '__slots__' list, which restricts the 
the list of attributes and prevents the creation of a dictionary
associated with every instance of Node.

Using a wrapper class could also allow exposing the wrapped data item's methods
and attributes in the future.

ClusterModel:
_____________


This interface class example allows for separation of data and code. 
A given clustering scheme may require many parameters, and it would be bad
practice to hard-code these. This class could provide a means to glue various
types of cluster model into one common interface, the parameters of which are
read from a separate data file (e.g. k_means_cluster_model.ini).


Sparse matrices:
________________


Some graph metric computations are conveniently performed on an adjacency matrix.
The Scipy sparse matrix class (specifically csr_matrix) is a good candidate,
as it is well-tested and allows convenient conversion to Numpy formats. 
