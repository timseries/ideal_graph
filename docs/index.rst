.. ideal_graph documentation master file, created by
   sphinx-quickstart on Sun Jul 20 23:40:15 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ideal_graph
=======================================

Contents:

.. toctree::
   :maxdepth: 2

   apireference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
