import os
from setuptools import setup, find_packages

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "ideal_graph",
    version = "0.0.1",
    author = "Timothy Roberts",
    author_email = "timothy.daniel.roberts@gmail.com",
    description = ("Ideal Graph package."),
    license = "BSD",
    keywords = "graph, network, coolness",
    url = "https://bitbucket.org/timseries/ideal_graph",
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    long_description=read('README.rst'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 2",
        "License :: Free To Use But Restricted",
    ],

)