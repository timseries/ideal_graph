API Reference
=============

Graph Interface
```````````````
.. autoclass:: ideal_graph.graph.Graph
    :members: 
    :special-members: 


Data Interface
``````````````
.. autoclass:: ideal_graph.node.Node
    :members:
    :special-members:

.. autoclass:: ideal_graph.clustering.ClusterModel
    :members: __init__

.. _clustering-algorithms:

Clustering Algorithms
`````````````````````
.. py:currentmodule:: ideal_graph.clustering
.. autofunction:: k_means


.. _shortest-path-algorithms:

Shortest Path Algorithms
````````````````````````
.. py:currentmodule:: ideal_graph.shortest_path
.. autofunction:: dijkstra


.. _minimum-spanning-tree-algorithms:

Minimum Spanning Tree Algorithms
````````````````````````````````
.. py:currentmodule:: ideal_graph.minimum_spanning_tree
.. autofunction:: kruskal

