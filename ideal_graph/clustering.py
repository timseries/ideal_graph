#!/usr/bin/python -tt
"""This module implements a collection of clustering algorithms and classes 
used in the ideal_graph package.

"""
from collections import defaultdict
import numpy as np
from numpy.linalg import norm
from numpy import min as nmin
import scipy.sparse as sp
import random

class ClusterModel(dict):
    """Cluster model class to read parameters from a file and interface to
    various clustering algorithms.
    """

    def __init__(self, parameter_file = None):
        """Constructor for :class:`ideal_graph.clustering.ClusterModel`.

        Initializes this storage class with the contents of `parameter_file` 
        (format TBD). Provides compatible interface with 
        :ref:`sklearn.cluster <sklearn:clustering>`.

        Args:
            parameter_file (:class:`str`): Path to a configuration file. 
                 Use defaults if :const:`None`.
        """
        
def k_means(feature_matrix, **kwargs):
    r"""Generate clusters using :math:`K`-means via Lloyd's algorithm [1]_.

    This supports both Numpy and Scipy (sparse) feature matrices, although
    currenlty a Scipy sparse feature matrix input runs much slower, and should
    only be used if `feature_matrix` can't fit in memory as a Numpy ndarray.

    Args:
        feature_matrix (:class:`numpy.ndarray` or 
            :class:`scipy.sparse.csr_matrix`): Feature matrix
            :math:`\mathbf{F}`.
            See :func:`ideal_graph.graph.Graph.get_feature_matrix`.
            
    Keyword Args:
        n_clusters (:class:`int`): The number of clusters to generate.
        
        tol (:class:`float`, optional): The convergence tolerance. Stops
            when :math:`\| \mathbf{m}_i - \mathbf{m}_{i-1} \|_2 < \mathrm{tol}`, 
            where :math:`\mathbf{m}_i` is the :math:`KF`-length vector
            of cluster means at iteration :math:`i`. Default 1e-4.
            
        max_iter (:class:`int`): Maximum number of iterations before stopping.
            Default 1000.
            
    Returns: 
        tuple(means, cluster_labels):
        
        `means` (:class:`numpy.ndarray` or :class:`scipy.sparse.csr_matrix`): \
        Matrix :math:`\mathbf{M}\in\mathbb{R}^{K \times F}`. Output clusters'
        features are centered on these mean features.
                
        `cluster_labels` (:class:`dict` of :class:`list`): The dictionary of 
        clusters. Each key is the row index of the `means` matrix, 
        :math:`\mathbf{M}`, and each corresponding list of labels are the
        row indices of the `feature_matrix` :math:`\mathbf{F}`.

    Note:     
        The algrorithm is currently seeded with a random sampling of the
        features, though there are more sophisticated ways to do this in order
        to avoid converging on bad local minima (i.e. k-means++ [2]_)

    References:
        .. [1] http://en.wikipedia.org/wiki/Lloyd's_algorithm
        .. [2] http://theory.stanford.edu/~sergei/papers/kMeansPP-soda.pdf
    """
    
    #################################
    #Initialization / Input checking#
    #################################
    n_clusters = kwargs['n_clusters']
    if kwargs.has_key('tol'):
        tolerance = kwargs['tol']
    else:    
        tolerance = 1e-4
    tolfun = lambda m1, m2 : norm(m1 - m2) <= abs(tolerance)

    if kwargs.has_key('max_iter'):    
        max_iterations = kwargs['max_iter']
    else:
        max_iterations = 1000    
        
    if n_clusters <= 1:
        raise ValueError('Number of clusters must be > 1')

    if sp.isspmatrix_csr(feature_matrix):
        vstack = sp.vstack
        to_np = lambda t : np.array(t.todense())
    elif isinstance(feature_matrix, np.ndarray):
        vstack = np.vstack
        to_np = lambda t : t
    else:
        raise TypeError('Input feature matrix is not an ndarray or csr_matrix')
    
    if feature_matrix.ndim != 2:
        raise ValueError('Input feature matrix is not a 2-dimensional')
    
    n_features = feature_matrix.shape[0]
    feature_size = feature_matrix.shape[1]
    if n_features <= 1 or feature_size == 0:
        raise ValueError('Bad input feature matrix shape: ' 
                         + str(feature_matrix.shape))

    if n_clusters > n_features:
        raise ValueError('Number of clusters > number of features: '
                         + str(n_clusters) + ' > ' + str(n_features))
    
    feature_iterator = xrange(n_features)
    cluster_iterator = xrange(n_clusters)
    minfun = lambda minvals: minvals[1] 
    
    # TODO(Tim): replace this with something cleverer, like k-means++    
    random.seed(0)
    means = to_np(feature_matrix[random.sample(feature_iterator, n_clusters), :])
    prev_means = (1 + 2 * tolerance) * means
    iterations = 0
    cluster_labels = defaultdict(list)
    #################
    #Start algorithm#
    #################
    while not tolfun(means, prev_means) and iterations < max_iterations:
        prev_means = means
        new_means = []
        clusters = defaultdict(list)
        cluster_labels = defaultdict(list)
        for feature_ix in feature_iterator:
            feature = feature_matrix[feature_ix,:]
            feature_rep = vstack([feature] * n_clusters)
            best_ix = norm(feature_rep - prev_means, axis = 1).argmin()
            clusters[best_ix].append(feature)
            cluster_labels[best_ix].append(feature_ix)
            
        for cluster_mean in cluster_iterator:
            new_means.append(vstack(clusters[cluster_mean]).mean(axis = 0))
            
        new_means = np.vstack(new_means)
        # if sp.issparse(feature_matrix):    
        #     new_means = sp.csr_matrix(new_means)
        means = new_means
        iterations += 1
    means = np.array(means)    
    return means, cluster_labels
