#!/usr/bin/python -tt
"""This module implements a collection of shortest path algorithms and classes 
used in the ideal_graph package.

"""

def kruskal(graph, **kwargs):
    r"""Find the minimum spanning tree of `graph` via Kruskal's algorithm [5]_.

    Kruskal's algorithm works on graphs to determine the minimum spanning trees,
    which are the subgraphs which span each connected component with
    minimal edge weight.

    Imagine driving between cities in Florida on a sales trip with a group of 
    colleagues in a shared van selling hurricane insurance. You all start at
    the same city, and at any particular city some of the team can branch
    off in a new vehicle. You wish to know which set of roads to use, given
    that you must visit all of the cities. Solving the minimum spanning tree 
    problem is just like solving this traveling sales team problem.
    Revisiting the shortest path example in 
    :func:`ideal_graph.shortest_path.dijkstra`, we add Miami to our list of 
    cities.

    .. image:: http://goo.gl/SBwfNU
    
    |
    
    We see that the relative driving times between cities (proportional to
    thickness) gives us the edge weights of our graph of cities.
    Kruskal's starts by selecting the lowest edge weight (or in the case
    of a tie, one of the lowest at random) such that a cycle isn't formed.
    Since Palm Coast (P) to Daytona Beach (D) is only a 30 minute drive time,
    we include this segment first. 
    
    .. image:: http://goo.gl/GSHg12
    
    |
    
    Jacksonville (J) to Palm Coast, and Sumterville (s) to Tampa (T), both
    take 60 minutes, so the second and third iteratons of this algorithm add 
    these segments (in arbitrary order).
    
    .. image:: http://goo.gl/G28iwF
    
    |
    
    The next shortest segment is from Palm Coast to Sumterville (110 minutes).
    
    .. image:: http://goo.gl/gcLjM4
    
    |
    
    Finally, the next shortest segment is from Daytona Beach to Tampa
    (120 minutes) and the shortest segment after that is Jacksonville to 
    Sumterville (130 minutes). Since both of these create cycles, neither are
    included, so the algorithm terminate by adding the path from Daytona Beach 
    to Miami.
        
    .. image:: http://goo.gl/F732tF
    
    |
    
    This algorithm is interesting because it works on graphs with multiple
    connected components, producing a set of mininimal spanning trees, (a 
    minimimal spanning forest), and has sub-quadratic runtime (in edges
    or vertices).

    Args:
        graph (unspecified graph class): The graph.
            
    Keyword Args:
            -
        
        `Future functionality.`
            
    Returns: 
        :class:`list` of `unspecified graph class`: See
            :func:`ideal_graph.graph.Graph.minimum_spanning_tree`, as an example.

    References:
        .. [5] http://www.cmat.edu.uy/~marclan/TAG/Sellanes/Kruskal.pdf
    """
    
    pass
