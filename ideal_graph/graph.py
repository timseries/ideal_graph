#!/usrrdered/bin/python -tt
r"""This module implements the Graph class component of 
the Ideal Graph package.

"""

from collections import defaultdict
from collections import OrderedDict
import numpy as np
import scipy as sp
from scipy.sparse import csr_matrix, dia_matrix


from node import Node
from clustering import k_means
    
class Graph(object):
    """Graph class for representing a directed or undirected 
    graph, with common routines for analysis on graphs.
                                                                                            
    Attributes:
        graph_properties (:class:`dict`): 
            A dictionary of named properties.

            **directed** (:class:`bool`, optional): 
                :const:`True` (default) if graph is directed. 
                :const:`False` otherwise.
            
            **simple_edges** (:class:`bool`, optional): 
                :const:`True` if edges do not require association with data. 
                :const:`False` (default) otherwise.

        edges(:class:`collections.OrderedDict` of \
              :class:`ideal_graph.node.Node` to \
              :class:`set` or :class:`dict`):  
    
            A dictionary of directional edges, where each entry represents an
            edge from a node (e.g. ``node1``), to one or 
            more nodes (e.g. ``node2,node3,node4``).

        edges_in (:class:`collections.OrderedDict` of \
                  :class:`ideal_graph.node.Node` to \
                  :class:`set` or :class:`dict`): 
                
            A dictionary of directional edges, used for directed 
            graphs only. For a given key in :attr:`edges_in`, (``node1``), the 
            corresponding value is the :class:`set` or :class:`dict` of nodes
            which have a directed edge toward ``node1``.

      """
    
    __slots__ = ['edges', 'edges_in', 'graph_properties'] #reduce mem
    
    
    def __init__(self, edges = None, **kwargs):
        """Class constructor for :class:`ideal_graph.graph.Graph`.

        Args:
        
            edges (:class:`dict` or :class:`numpy.ndarray` or \
                :class:`scipy.sparse.csr_matrix`): If a :class:`dict` 
                (or subclass thereof), must conform to :attr:`edges`
                attribute. If a matrix, entries must
                follow the definition in 
                :func:`ideal_graph.graph.Graph.get_adjacency_matrix`.

        Keyword Args: 
            -
        
        See :attr:`graph_properties`. 

        Examples:
            In these examples, ``nodeX``, ``X in {1...5}``, indicates
            a :class:`ideal_graph.node.Node` hash (key)
            or a reference to a :class:`ideal_graph.node.Node` (value),
            for simplicity.
        
            **Example 1**: 
            To create a simple-edged directed graph, :attr:`simple_edges`
            and :attr:`directed` are both initialized to :const:`True`. Here, 
            elements of a :class:`set` encode edges::

                >>> edges = {node1 : set([node2, node3, node4]),
                             node2 : set([node3, node4]),
                             node3 : set([]),
                             node4 : set([]),
                             node5 : set([])}

                >>> mygraph = Graph(edges, simple_edges = True)

                >>> mygraph.edges_in

                {node1 : set([]),
                 node2 : set([node1]),
                 node3 : set([node1, node2]),
                 node4 : set([node1, node2]),
                 node5 : set([])}
                 
            **Example 2**:
            To create a non-simpled-edged, directed version of **Example 1** 
            (edges have data), initialize 
            :attr:`graph_properties` ``[simple_edges]``
            to :const:`False` (default) and 
            :attr:`graph_properties` ``[directed]`` to 
            :const:`True` (default). 
            Here, a :class:`dict` is used to encode the edges and edge data, 
            where ``edgedataY``, ``Y in {2..6}``, is a reference to any
            arbitrary data::

                >>> edges = {node1 : {node2: edgedata2,        
                                      node3: edgedata3,        
                                      node4: edgedata4},       
                             node2 : {node3: edgedata5,        
                                      node4: edgedata6},         
                             node3 : {},                       
                             node4 : {},                       
                             node5 : {}}

                >>> mygraph = Graph(edges)                                
        
                >>> mygraph.edges_in

                {node1 : set([]),
                 node2 : {node1: edgedata2},
                 node3 : {node1: edgedata3,
                          node2: edgedata5},
                 node4 : {node1: edgedata4,
                          node2: edgedata6},
                 node5 : {}}

        
            **Example 3**:
            To create a simple-edged, undirected graph, initialize
            :attr:`graph_properties` ``[simple_edges]`` to :const:`True` and
            :attr:`graph_properties` ``[directed]`` to 
            :const:`False`. **Example 1** becomes the following::

                >>> edges = {node1 : set([node2, node3, node4]),
                             node2 : set([node3, node4]),
                             node3 : set([]),
                             node4 : set([]),
                             node5 : set([])}

                >>> mygraph = Graph(edges, simple_edges = True, directed = False)                                
        
                >>> my_graph.edges

                {node1 : set([node2, node3, node4]),
                 node2 : set([node1, node3, node4]),
                 node3 : set([node1, node2]),
                 node4 : set([node1, node2]),
                 node5 : set([])}

                >>> my_graph.edges_in

                None
                
            .. note:: The constructor automatically fixes the entries for 
                ``node1``, ``node2``, ``node3``, and ``node4`` in **Example 3**.


        """
        self.graph_properties = {'simple_edges': False,
                                 'directed': False}

        if kwargs is not None:
            self.graph_properties.update(kwargs)
        
        if edges is not None:
            # Create self.edges here appropriately with type checking on `edges`.
            pass
        
        if self.graph_properties['directed']:
            # Create self.edges_in here from self.edges
            pass


    def get_adjacency_matrix (self): 
        r"""Generate an adjacency matrix of this graph.
        
        Generates the adjacency matrix 
        :math:`\mathbf{A}\in\mathbb{R}^{N \times N}`
        which encodes edge weights. The :math:`i,j^\mathrm{th}` entry of 
        :math:`\mathbf{A}`, :math:`A_{ij}`, encodes the presence
        (non-zero floating-point weight value) or absence (0) of an edge 
        from node :math:`i` to node :math:`j`. If :attr:`edges` contain edge 
        data other than numerical edge weights, assume all edge weights
        are equal (1.0).

        Returns:
            :class:`scipy.sparse.csr_matrix`: The adjacency matrix
            :math:`\mathbf{A}` as defined above.
                         
        """

    def get_feature_matrix (self, matrix_class = 'auto'): 
        r"""Generate a feature matrix of this graph.

        Returns the feature matrix 
        :math:`\mathbf{F}\in\mathbb{R}^{N \times F}`, 
        where :math:`N` is the number of 
        nodes in this graph, and :math:`F` is the length of the 
        feature vector :math:`\mathbf{f}_n` associated with each 
        node labelled :math:`n`, and 
        :math:`\mathbf{f}_n \in \mathbb{R}^{F} 
        \;\;\forall \;\;n \in \{0 \ldots N-1\}`. Each feature vector
        :math:`\mathbf{f}_n` forms the :math:`n^{\mathrm{th}}` row of 
        :math:`\mathbf{F}`.
        
        Args:
            matrix_class(:class:`str`): Either 'ndarray', 'csr_matrix', 
                or 'auto'. If 'auto', automatically determines the most
                memory-efficient class. 

        Returns:            
            :class:`scipy.sparse.csr_matrix` or :class:`numpy.ndarray`: If 
            nodes are representable by a feature matrix :math:`\mathbf{F}` 
            as defined above. Otherwise return :const:`None`.
            
        """
        
    def get_subset(self, subset_list = None): 
        r"""Return a subset of this graph

        Args:
            subset_list(:class:`list` of :class:`int`, optional): A list of
                 indices which are the positions of the nodes in this 
                 graph's :attr:`edge`.

        Returns:
            :class:`ideal_graph.graph.Graph`: A subset of this graph.
        
        """
        pass 
    
    def out_degree(self, node = None):
        r"""The number of edges to other nodes. 
        
        Args:
            node (:class:`ideal_graph.node.Node`, optional): The node 
                whose out-degree is calculated.
                
        Returns:
            :class:`int` or :class:`scipy.sparse.dia_matrix`: A non-negative
                integer representing the out-degree of `node`. If node is None, 
                return the out-degree for the entire graph as a diagonal matrix
                of integers. If graph is undirected, this is simply the degree.
        """
        # Suggested implementation: O(n) -> len(self.edges[node])
        pass
        
    def in_degree(self, node = None):
        r"""The number of edges from other nodes to 'node'. 
        
        Args:
            node (:class:`ideal_graph.node.Node`, optional): The node 
                whose in-degree is calculated. 

        Returns: 
            :class:`int` or :class:`scipy.sparse.dia_matrix`: A non-negative
                integer representing the in-degree of `node`. If node is None, 
                return the in-degree for the entire graph as a diagonal matrix
                of integers.  If graph is undirected, returns 
                :func:`ideal_graph.graph.Graph.out_degree`.
            
        """
        # Suggested implementation (O(n)): 
        # Self.out_degree[node] for undirected
        # len(self.edges_in[node]) for directed
        
        pass
        
    def shortest_path(self, node_begin, node_end, **kwargs):
        r"""Calculate the shortest path(s) from `node_begin` to `node_end`.

        Args:
            node_begin (:class:`ideal_graph.node.Node`): The node which defines
                the beginning of the shortest path.
                
            node_end (:class:`ideal_graph.node.Node`): The node which defines
                the end of the shortest path.
                
        Keyword Args:
            algorithm (:class:`str`): The algorithm used to find the shortest
                path. One of:
                'astar' (default),'dijkstra','floyd_warshall', 'bellman_ford',
                or 'johnson'.
                If algorithm choice is incompatible with this graph, 
                this method tries to automatically choose the optimum algorithm.

        Returns:     
            :class:`list` of :class:`ideal_graph.graph.Graph`: If no path 
            exists, an empty list is returned. If multiple shortest 
            paths exist, return a list of Graphs of the equally
            shortest paths.
        
        Examples:     
            Simple of a simple-edged, directed graph, ``mygraph``::
                
                            node4 ----> node5
                              ^           ^
                              |           |
                              |           v
                node1 ----> node2 ----> node3

            Shortest path would return::

                >>> mygraph.shortest_path(node1, node3)
            
                [Graph(OrderedDict([(node1:set(node2)),
                                    (node2:set(node3,node4)),
                                    (node3:set(node5))]))]

                >>> mygraph.shortest_path(node2, node5)
            
                [Graph(OrderedDict([(node2:set(node3,node4)),
                                    (node4:set(node5)),
                                    (node5:set(node3))])),
                 Graph(OrderedDict([(node2:set(node3,node4)),
                                    (node3:set(node5)),
                                    (node5:set(node3))]))]

                >>> mygraph.shortest_path(node2, node1)
            
                [Graph(None)]
        
        See Also:
            :ref:`shortest-path-algorithms`.
                            
        """
        # Suggested implementation: For Dijkstra's algorithm, use a priority
        # queue or heap queue, which is a popular and optimal datastructure for
        # this algorithm. http://www.cs.sunysb.edu/~rezaul/papers/TR-07-54.pdf
        # Python's heapq is one possible package.
        # The other algorithm options are variants of Dijkstra's
        # and therefore use a priority queue.
        pass

    def add_node(self, data = None, children = None, parents = None):
        r"""Add a node to the graph, optionally specifying
        edges pointing to and away from the added node.

        Args:
            data(:class:`ideal_graph.node.Node` or abitrary data, optional): \
                Entry to be added to the graph. If abitrary data or None, 
                instantiate a new :class:`ideal_graph.node.Node`. 
                
            children(:class:`dict`, optional): Keys are nodes which
                this node points to, and values are data 
                associated with corresponding edges. If graph is 
                simple-edged, then values in dict are ignored.
        
            parents(:class:`dict`, optional): Keys are nodes pointing
                to this node, and values are data associated with
                corresponding edges. If graph is simple-edged, 
                then values in are ignored. If graph is undirected,
                this input is ignored completely.

        Returns:
            :const:`None`
            
        """
        #Suggested implementation: Update self.edges
        #and self.edges_in if necessary. Call self.add_edge if necessary.
        
        pass
                                                                                
    def remove_node(self, node, return_node = False):
        r"""Remove a `node` from the graph.
        
        Args:
            node (:class:`ideal_graph.node.Node`): Node to be removed from this
                graph. This can be a reference to the node instance, 
                :attr:`unique_label` of the node.

            return_node (:class:`bool`): Return a copy of the node to be removed
                if :const:`True`. Return :const:`None` otherwise (default).

        Returns:
            :class:`ideal_graph.node.Node`: Copy of removed node, 
                or :const:`None`.
         """  
         
        # Suggested implementation: Remove node from self.edges
        # and self.edges_in (if necessary) using del. 
        # Call self.remove_edge if necessary.
         
        pass

    def add_edge(self, node1, node2, data = None):
        r"""Add an edge from `node1` to `node2`.
        
        Args:
            node1 (:class:`ideal_graph.node.Node`): The node where the 
                edge starts.
            node2 (:class:`ideal_graph.node.Node`): The node where the 
                edge ends.
            data (arbitrary data, optional): Data associated with this edge.

        .. note:: For an undirected graph, the ordering of node1 and 
            doesn't matter: add_edge(node1, node2) produces the same
            effect as add_edge(node2, node1).

        Returns:
            :const:`None`
        """ 
         
        # Suggested implementation: Update values of self.edges
        # and self.edges_in (if necessary).

        pass 

    def remove_edge(self, node1, node2, **kwargs):
        r"""Remove the edge pointing from node1 to node2.

        For an undirected graph, the ordering of `node1` and `node2`
        doesn't matter. In this case,
        remove_edge(`node1`, `node2`) produces the same effect as 
        remove_edge(`node2`, `node1`).

        Args:
            node1 (:class:`ideal_graph.node.Node`): Node where the edge starts.
            node2 (:class:`ideal_graph.node.Node`): Node where the edge ends.
        
        Keyword Args:
            -
        
        `Future functionality.`
        
        Returns:
            arbitrary data or :const:`None`: The data associated with 
            this edge if :attr:`graph_properties` ``[simple_edges]`` is
            :const:`False`, otherwise :const:`None`.
        """ 

        # Suggested implementation: From self.edges[node1], pop node2 value.
        # Remove corresponding entry in self.edges_in (if necessary). 
        # Call self.remove_edge if necessary.

        pass 
        
    def find(self, search_node, **kwargs):
        """Search the graph for `search_node`.
        
        Args:
            search_node(:class:`ideal_graph.node.Node` or arbitrary data):\
                The node or data to be searched. If 
                the input class is :class:`ideal_graph.node.Node`, 
                then the keys of :attr:`edges` is searched.
                Otherwise the arbitrary data must implement an 
                equality operator.
            
        Keyword Args:
            -
        
        `Future interface additions.`
        
        Returns:
            :class:`ideal_graph.node.Node`: The node which matches `search_node`.
        """ 

        # Suggested implementation: search_node in self.edges, if search_node
        # is a Node. Otherwise compare the Node.data_item to the search_node
        # value (linear in the number of keys of self.edges).

        pass 

    def traversal(self, start_node, **kwargs):
        """Traverse the graph, starting with `start_node`.

        Args:
            start_node(:class:`ideal_graph.node.Node`): Node to start the 
                traversal.
            
        Keyword Args:
            algorithm(:class:`str`): The algorithm used to find the minimum 
                spanning tree, one of:
                'depth_first' (default) or 'breadth_first'.
        
        Returns:
            :class:`ideal_graph.graph.Graph`: A subgraph of this instance, which
                represents the connected component (tree) of which `start_node`
                is the root, and whose entries in :attr:`edges` have the same
                order as the traversal (first visit).
        """ 

        # Suggested implementation: 

        # depth_first: Use recursion.
        # breadth_first: Use a Queue and proceed iteratively.

        pass 
    
    def minimum_spanning_tree(self, **kwargs):
        r"""Returns the minimum spanning tree for each of the connected
        components of this graph. If edge weights cannot be compared or are
        not present, simply finds the spanning tree(s). 
            
        Keyword Args:
            algorithm(:class:`str`): The algorithm used to find the minimum 
                spanning tree, one of:
                'kruskal'(default), 'prim', 'boruvka', 'reverse_delete',
                If algorithm choice is incompatible with this graph, 
                choose deafult.
            first_only(:class:`bool`, optional): Only return the first minimum
                 spanning tree for each connected componentent. Default True.

        Returns:
            :class:`list` of :class:`ideal_graph.graph.Graph`: Each graph in 
                this list corresponds to the minimum spanning tree of each
                connected component of this graph.
                
        See Also:
            :ref:`minimum-spanning-tree-algorithms`.        

        """
        # Suggested implementation: Use this Graph class to build each
        # minimum spanning tree of each connected component of this graph.

        pass

    def centrality(self, node=None, **kwargs):
        """Computes the for a given node. If no node is given, compute the
        centrality across the entire graph and return node with the maximum.
        
        Args:
            node(:class:`ideal_graph.node.Node`, optional): Compute the 
                centrality for a given node. Optional.
            
        Keyword Args:    
                type(:class:`str`): The type of centrality to compute, one of:
                    'degree' (default), 'degree_in', 'degree_out', 'closeness',
                    'betweeness', 'eigenvector',
                    'katz', 'pagerank', or 'percolation'. 
                    If the graph is undirected, 
                    'degree_in' and 'degree_out' both default to 'degree'.

        Returns:
            tuple(centrality, node):

            `centrality` (:class:`float`): A number representing the centrality.
            
            `node` (:class:`ideal_graph.node.Node`): A node which has 
            `centrality` measure.
        """
        
        # Suggested implementation: An adjacency matrix representation of 
        # this graph should be available for some centrality computations.
        # A good data structure for this is the Scipy sparse matrix.
        
        pass
    
    def connectedness(self, **kwargs):
        r"""Computes the connectedness (connectivity) of a graph.
        
        Keyword Args:
            type (:class:`str`): The type of connectedness to compute, one of:
                'vertex'(default), 'edge', or 'algebraic'. 
                If the connectedness choice is incompatible with this
                graph, use default.
            algorithm (:class:`str`): The algorithm to use. 
                One of 'dinic' (default), 'ford_fulkerson', 'edmonds_karp'.
                For algebraic connectivity this input is ignored, and 
                computations are performed on the Laplacian matrix of this graph.
                
        Returns:
            :class:`float`: Number representing the connectedness of this graph. 
        """
        
        # Suggested implementation: An adjacency matrix representation of 
        # this graph should be available for some connectedness computations.
        # A good data structure for this is the Scipy sparse matrix.
        
        pass

    def clustering(self, algorithm, model, **kwargs):
        r"""Generates a clustering of this graph.
        
        Args:
            algorithm (:class:`str`): The name of the algorithm to use.
                One of 'slink','clink','upgma','k_means' (default),
                'expectation_maximization', 'optics', 'dbscan', 'mean_shift',
                'clarans', 'birch', '4c', 'eric', 'affinity_propagation'.
                
            model (:class:`dict`): The clustering parameters.

        Keyword Args:
            -
        `Future functionality.`
                                
        Returns:
            tuple(central_nodes, clusters):

            `central_nodes` (:class:`list` of :class:`Graph`): The centers of
                the corresponding clusters. These aren't contained in this graph.
                
            `clusters` (:class:`list` of :class:`ideal_graph.node.Node`): \
                Subsets of this graph which represent clusters.
        
        See Also:
            :ref:`clustering-algorithms`.        
        """
        
        if algorithm == 'k_means':
            feature_matrix = self.get_feature_matrix()
            means, cluster_labels = k_means(feature_matrix, model)
            clusters = [self.get_subset(labels[1]) 
                        for labels in cluster_labels.iteritems()]
            central_nodes = [Node(means[labels[0],:]) 
                             for labels in cluster_labels.iteritems()]
        else:    
            raise ValueError('Unsupported algorithm: ' + str(algorithm))
        
        return (central_nodes, clusters)
        