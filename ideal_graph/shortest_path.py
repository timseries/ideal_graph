#!/usr/bin/python -tt
"""This module implements a collection of shortest path algorithms and classes 
used in the ideal_graph package.

"""

def dijkstra(graph, node_begin, node_end, **kwargs):
    r"""Find the shortest path from `node_begin` to `node_end` in `graph`
    via Dijkstra's algorithm [3]_.

    Dijkstra's algorithm works on directed graphs with non-negative edge weights
    to find the shortest path (minimum total edge weight) between two nodes in 
    the graph. 

    An analogy would be |driving_from_Jacksonville_to_Tampa|.
    What's the quickest way to get there? While this subject is hotly debated
    amongst Floridians, we can view this as a shortest path problem
    where the network of all of the cities in the United States (nodes)
    and roads that connect them (edges) represent our graph. The edge weight for
    a particular highway between two cities is the travel time, which is a
    function of the speed limit, traffic, roadwork, etc.

    Dijsktra's algorithm would start in Jacksonville, and consider all of the 
    immediate neighboring cities, and assign each one a provisional travel time:

    To simplify things, consider Palm Coast (P, driving time 60 minutes) and
    Sumterville (S, driving time 130 minutes) to be the only two immediate
    neighbors of Jacksonville (J). In the image below, the edge thicknesses
    reflect the relative driving times which are provisionally
    assigned by Dijsktra's:
    
    .. image:: http://goo.gl/GvbVQe
    
    |
    
    The next city to consider is the one with the smallest driving time from 
    Jacksonville, Palm Coast. Palm Coast  has as immediate neighboring cities
    Sumterville and Daytona Beach (D). 
    Showing these new paths from Jacksonville in blue, we have:

    .. image:: http://goo.gl/7ZjDRk
    
    |
    
    Note that this new provisional time from Jacksonville to Sumterville via
    Palm Coast is 170 minutes, which is 40 minutes longer than the time from
    Jacksonville to Sumterville directly. Therefore Dijsktra's sticks with the
    old path and time to Sumterville. Next, Jacksonville's other neighbor is
    considered, Sumterville, which only has one unvisited city, Tampa (T).
    The total time from Jacksonville to Tampa via Sumterville is 190 minutes.

    .. image:: http://goo.gl/YyVrCB
    
    |
    
    The algorithm isn't done yet, as it hasn't considered the remaining cities
    and possible paths which lead to Tampa. The driving time from Daytona to 
    Tampa (Daytona's only neighboring city) is 120 minutes. This gives a total
    time from Jacksonville (via Palm Coast and Daytona) to Tampa of 210 minutes.

    .. image:: http://goo.gl/D8x4WG
    
    |
    
    Therefore quickest way is Jacksonville to Sumterville to Tampa (190 minutes).
    
    This algorithm is interesting, as it is the basis for state-of-the-art 
    shortest path algorithms (such as A* and variants [4]_), and yet it was 
    first conceived in the 50's. It has found widespread use in many applications
    such as pathfinding, transportation, and Bacon numbers :-).

    .. |driving_from_Jacksonville_to_Tampa| raw:: html

        <a href="http://goo.gl/FH2egH" target="_blank">driving from Jacksonville to Tampa</a>
    
    Args:
        graph (:class:`ideal_graph.graph.Graph`): The graph.
        node_begin (:class:`ideal_graph.node.Node`): The node which defines
            the beginning of the shortest path.
                
        node_end (:class:`ideal_graph.node.Node`): The node which defines
            the end of the shortest path.
            
    Keyword Args:
            -
        
        `Future functionality.`
            
    Returns: 
        :class:`list` of `unspecified graph class`: See
            :func:`ideal_graph.graph.Graph.shortest_path` as an example.

    References:
        .. [3] http://www-m3.ma.tum.de/foswiki/pub/MN0506/WebHome/dijkstra.pdf
        .. [4] http://en.wikipedia.org/wiki/A-star_algorithm
    """
    
    pass
