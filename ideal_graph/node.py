# -*- coding: utf-8 -*-
#!/usr/bin/python -tt
r"""

This module implements the classes used as graph components 
the Ideal Graph package.

"""

from uuid import uuid4
    
class Node(object):
    r"""Node class for storing arbitrary data in a graph.

    Node is a wrapper class for a `data_item` (arbitrary data) with
    a :ref:`hashable <python:glossary>` `unique_label`.

    Attributes:
    
        data_item (arbitrary data): Instance of any object or built-in, which
            an instance of this class is meant to wrap.
        unique_label (hashable): A :ref:`hashable <python:glossary>` 
            label for this node.
    
    """

    __slots__ = ['data_item', 'unique_label'] 
    r"""The available attributes is listed here to save resources
    """
    
    def __init__(self, data_item, unique_label = None):
        """Class constructor.

        Args: 
            data_item (abitrary_data): See :attr:`data_item`.

            unique_label (hashable): See :attr:`unique_label`. If :const:`None`,
                generate a unique label for this node automatically using
                :func:`uuid.uuid4`.
        """

        
        self.data_item = data_item
        
        if unique_label is not None:
            self.unique_label = unique_label
        else:
            self.unique_label = uuid4()

    def __eq__(self, node_rhs):
        r"""Equality operator for comparing this instance to another.

        Args:
            node_rhs(:class:`ideal_graph.node.Node`): Node compared to this
                instance.   
                
        Returns:
            equality(:class:`bool`): The result of the equality test.
        """
        
        return hash(self.unique_label) == hash(node_rhs.unique_label)

    def __hash__(self):
        r"""Hash operator for creating a hash value for this instance

            Returns:
                hash(:class:`int`): The hash of this instance's 
                unique label.
        """
        
        return hash(self.unique_label)
