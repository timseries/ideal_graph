import os
import numpy as np

def two_d_clusters(centers, standard_deviations):
    """Draw truncated 2-D Gaussians in the range ([-1,1],[-1,1])
    """
    np.random.seed(0)
    num_samples = 210
    num_clusters = len(standard_deviations)
    cluster_size = num_samples / num_clusters
    feature_matrix = []
    for ix in xrange(num_clusters):
        center = centers[ix]
        std_dev = standard_deviations[ix]
        cluster = []
        while len(cluster) < cluster_size:
            x1, x2 = np.array([np.random.normal(center[0], std_dev), 
                               np.random.normal(center[1], std_dev)])
            if abs(x1) < 1 and abs(x2) < 1:
                cluster.append([x1, x2])
        feature_matrix.extend(cluster)
    feature_matrix = np.array(feature_matrix)[:num_samples]
    return feature_matrix