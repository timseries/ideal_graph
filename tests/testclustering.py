import numpy as np
from scipy.sparse import csr_matrix
import tests.datasets as datasets
import ideal_graph.clustering as clustering

import datasets
from .util import assert_absolute_error

def test_k_means():
    """Test the k-means algorithm for accuracy.
    """
    input_classtypes = [np.array, csr_matrix]
    centers = [(-.5,-.25), (0.01,0.7), (.7,-.25)]
    standard_deviations = [0.01, 0.5, 0.2]
    feature_matrix = datasets.two_d_clusters(centers, standard_deviations)
    absolute_error_thresh = 2e-1
    convergence_tolerance = 1e-10
    model = {'n_clusters': len(centers), 
             'tol': convergence_tolerance}
    row_sorter = lambda t: tuple(t)
    centers = np.array(sorted(centers, key = row_sorter))
    
    for input_classtype in input_classtypes:
        feature_matrix = input_classtype(feature_matrix)
        means, cluster_labels = clustering.k_means(feature_matrix, **model)
        #rearrange means to compare with centers
        means = np.array(sorted([mean for mean in means], key = row_sorter))
        assert_absolute_error(means, centers, absolute_error_thresh)
