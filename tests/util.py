import numpy as np

THRESHOLD = 1e-6

def assert_absolute_error(a, b, threshold = THRESHOLD):
    max_distance = np.abs(a - b).max()
    if max_distance <= threshold:
        return
    raise AssertionError(
            'The maximum elementwise difference is {0} '
            'which is greater than the threshold of {1}'.
            format(max_distance, threshold))

def assert_relative_error(a, b, threshold = THRESHOLD):
    """The maximum error relative to b.
    """
    max_distance = (np.abs(a - b) / b).max()
    if max_distance <= threshold:
        return
    raise AssertionError(
            'The maximum relative elementwise difference is {0} '
            'which is greater than the threshold of {1}'.
            format(max_distance, threshold))
